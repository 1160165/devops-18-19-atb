
CA2-PART1
--------

This is a demo of a chat aplication using java

-------------------------
1)- Create branch master

Create a folder named CA2 and push to Bitbucket.
Download the basic-demo at the https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/ and move to folder CA2\part1
Commit the initial demo.

    % - git add . 
	% - git commit -a -m "clone graddle project from bitbucket"
    
2)- Build the demo-aplication

Build
-----

To build a .jar file with the application:

    % ./gradlew build 

Run the server
--------------

Open a terminal and execute the following command from the project's root directory:

    % - java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>

Substitute <server port> by a valid por number, e.g. 59001

Run a client
------------

Open another terminal and execute the following gradle task from the project's root directory:

    % - ./gradlew runClient

The above task assumes the chat server's IP is "localhost" and its port is "59001". If you whish to use other parameters please edit the runClient task in the "build.gradle" file in the project's root directory.

To run several clients, you just need to open more terminals and repeat the invocation of the runClient gradle task

3)- Add a new task to execute the server.

Go to build.gradle and add the following task

    task runServer(type:JavaExec, dependsOn: classes){
        classpath = sourceSets.main.runtimeClasspath

        main = 'basic_demo.ChatServerApp'

        args '59001'
        }

Then commit to fixes the task:

	% - git commit -a -m "Fixes#7 - Add a new task to execute the server"

4)- Add a simple unit test and update the gradle script so that it is able to execute the test

Go to build.gradle and add the junit dependency:

    dependencies {
        testCompile 'junit:junit:4.12'
    }

Add a new folder test. Then create the class AppTest on folder test and add the following test

    package basic_demo;
    import org.junit.Test;
    import static org.junit.Assert.*;

    public class AppTest {

    @Test 
    public void testAppHasAGreeting() {
    App classUnderTest = new App();
    assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }

5)- Add a new task of type Copy to be used to make a backup of the sources of the application.
The sources content were copied to a new backUp folder

    task backUpCopy (type:Copy){
        from 'src'
        into 'backUp'
    }

6)- Add a new task of type Zip to be used to make a an archive (i.e., zip file) of the sources of the application.
The sources content were added to a new zip folder

    task packageDistribution (type:Zip){
        archiveFileName = "file.zip"
        destinationDirectory = file ("zip")
        from 'src'
    }

To end the part1 was created a tag ca2-part1 to mark this, by command:

    % - git tag ca2-part1
    % - git commit -a -m "Fixes#10"
    % - git push origin master ca2-part1
    

CA2-PART2
--------

This projects demonstrates how to integrate Plantuml with Gradle in order to integrate 
technical diagrams produced with Plantuml in javadoc.

1)- Render PlantUml - PlantUml-Gradle

First created a branch to execute this part of assignment, doing the command:

    % - git branch gradle-plantuml.


To render a PlantUml file need a specific task. This can be done with the task called renderPlantUml. 
To execute just need to8 type (in a terminal/console):

    % - gradlew renderPlantUml
  
By default, the images are produced into the folder build/puml

Since the project is a java project there is a javadoc task that can be executed to generate javadoc.

    % - gradlew javadoc

Then we need to automated this task, because the images by default were moved to build/puml, and we need to move to javadoc folder at build/docs/javadoc/com/twu/calculator to show images at overview.html.
We can to this by creating a new task Copy.

    task copyPlantUml(type: Copy) {
        from 'build/puml'
        into 'build/docs/javadoc'
        include '**/*.png'
    }
   

The generated images will be automatically forwarded to the file described above. A commit was then made with these new changes.

    % - git commit -a -m "Fixes#11 - Solve issue"
    % - git push

2)- Render PlantUml - Errai-Demonstration-Gradle

Since we have not implemented the rendering classes of the plantuml files as in the previous exercise, the solution used was through a specific plugin that internally has methods to rendering uml files. The plugin referred to is called PlantUMLPlugin, and was placed in the build.gradle file in the plugins session as follows:

    id "io.kristiansen.gradle.PlantUMLPlugin" version "0.0.1"
    
Then we to create a folder assets (by default the uml files needs to be there) and create sequence diagram and class diagram. So we can do the task "gradlew renderPlantUml" and the generated png and svg were created in assets.

To show this images at index.html, we also need to "copy" the images to javadoc folder.

First, to create the javadoc, we can do the task:

    % - gradlew javadoc
    
Then, we do the following Copy task:

    task copyPlantUml (type:Copy){
        from'assets'
        into 'build/docs/javadoc'
        include '**/*.png'
    }

The images were copied to build/docs/javadoc. But to show the images at overview-summary.html, we also need to modify this file, adding the specif html tags.


CA2-Alternative Tecnology
--------

Grails is an open source web application framework that uses the Apache Groovy programming language (which is in turn based on the Java platform). It is intended to be a high-productivity framework, providing a stand-alone development environment and hiding much of the configuration detail from the developer.

The main goal of Grails is to create a high productivity web framework for the Java platform. For this, it uses technologies considered mature in the Java world, such as the Hibernate and Spring frameworks, through an interface that seeks to be simple and consistent. The framework isolates the developer from the complex details of data persistence and incorporates the MVC development standard in a natural way. It also provides web templates for easy implementation of the user interface and support for Ajax programming.

Grails is built on the top of the Java platform which makes it natural to integrate a Grails application with libraries, frameworks, and Java code.

1)- Generate docs

To generate docs on grails only needs to to the follow command:

	% - grails docs
	
Than it will automatically create docs of program to a new folder C:\Users\vinic\Documents\Vinicius\Switch\DevOps\Trabalho\Exercicios\ca2\part2\grail-web-demonstration\target\docs

2)- Render PlantUml

To render plantuml needs to install a plugin specific to grails. The plugin used was "UML Class Diagram Plugin". On BuildConfig.groovy we need to add:

    % - runtime "org.grails.plugins:uml-class-diagram:0.4.4"
    
        grails.project.fork = [
                test: false ,
                run: false ,
                war: false ,
                console: false ,
        ]
        grails.reload.enabled = true
        grails.plugin.location.'uml-class-diagram'="../grails-plugin-uml-class-diagram"


Online
---
On file Config.groovy also needs to do this configuration

    grails.plugin.'uml-class-diagram'.fieldFilterRegexps=['^id$','^version$']
    grails.plugin.'uml-class-diagram'.classFilterRegexps=['.*teste']
    grails.plugin.'uml-class-diagram'.diagramType='DOMAIN'
    grails.plugin.'uml-class-diagram'.showCanonicalJavaClassNames=false
	
We can now run the app choosing the port

	% - grails -Dserver.port=xxxx run-app
	
When the webapp is running, the UML diagrams are exposed through the following URL:
http://server:port/web/uml

![Wizard example](part2/grail-web-demonstration/grails-app/assets/0.4.3-wizard.png)

Offline
---

The UML diagrams can be generated before runtime doing:

    % - grails uml-class-diagram
