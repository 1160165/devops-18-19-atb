# Docker Compose VM Demonstration

This project demonstrates how to run a container with Wildfly and other with H2.

## Requirements

Install Docker in you computer.

Step 1 - Docker Compose
-------------
First of all I had to follow the instructions on Moodle at the file Switch - Curso de Especialização em Desenvolvimento de Software
Lecture 07b.
The example we are following is https://github.com/atb/docker-compose-demo, so I had to download this file and move to the folder ca4.
Next I copied the errai-demonstration-gradle.war (the same as the ca3 exercise) to the folder ca4/web/war.

1) Start the DockerCompose
Then to start with Dockers first I had to download Docker Compose at: https://docs.docker.com/compose/install/.
The second step is to open the Docker Quickstart Terminal and install for the first time.

On terminal, when we write "docker-compose up" we create the containers and get it all running. But as we had not defined the Dockerfile, the containers were empty and we cannot do mutch thing with. To fix that, we had to edit the Dockerfile.

Step 2 - Edit Dockerfile
-------------

		FROM ubuntu

		RUN apt-get update && \
			apt-get install -y openjdk-8-jdk-headless && \
			apt-get install unzip -y && \
			apt-get install wget -y

		RUN mkdir -p /usr/src/app

		WORKDIR /usr/src/app/

		RUN wget http://download.jboss.org/wildfly/15.0.1.Final/wildfly-15.0.1.Final.tar.gz && \
			tar -xvzf wildfly-15.0.1.Final.tar.gz && \
			sed -i 's#<inet-address value="${jboss\.bind\.address:127\.0\.0\.1}"/>#<any-address/>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
			sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
			sed -i 's#<password>sa</password>#<password></password>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml

		COPY war/errai-demonstration-gradle.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/

		CMD /usr/src/app/wildfly-15.0.1.Final/bin/standalone.sh
		
Then on terminal I wrote the following commands:

	1-To build the new image:
	
		docker-compose build
		
	2-To get all started
		
		docker-compose up
	
Done that, we can open a new terminal on windows and if we write "docker-machine ip" we get our IP that containers were located.

Opening the browser and putting the adress "http://192.168.99.100:8080" we can see the errai-demonstration and then create new contacts and save.
Opening the browser and putting the adress "http://192.168.99.100:8082" we can see the database we have attached and see if datas were persisted.

As we had edited the Dockerfile like was demonstrated, both tasks were successfully done. 

Step 3 - Put the images on DockerHub
-------------

First I had to signup on DockerHub and create an account.
Opening again the docker terminal, we wrote the following commands:

	1-Log into the Docker Hub from the command line
	
		docker login --username=1160165 --email=1160165@isep.ipp.pt
		
	2-Check the image ID using
	
		docker images
	
	3-And tag my image
	
		docker tag imageid 1160165/ca4_web:web
		docker tag imageid 1160165/ca4_db:db
		
	4-Push my image to the repository I created
	
		docker push 1160165/ca4_web
		docker push 1160165/ca4_db
		
The URLs with the images created are the following

Web: https://cloud.docker.com/repository/docker/1160165/ca4_web

Database: https://cloud.docker.com/repository/docker/1160165/ca4_db 

