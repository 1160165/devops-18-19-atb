package org.atb.errai.demo.contactlist.client.shared;

import org.junit.Test;

import static org.junit.Assert.*;

public class ContactTest {

    @Test
    public void createNewContactEmptyConstructor() {
        //Assert
        String name = "Vinicius";
        String phone = "936932448";
        String email = "vinicius_franco5@hotmail.com";
        String postalCode = "4200013";
        Contact contact = new Contact();
        //Act
        contact.setName(name);
        contact.setPhone(phone);
        contact.setEmail(email);
        contact.setPostalCode(postalCode);
        String result = contact.getName();
        String expectedResult = "Vinicius";
        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void createNewContactGetName() {
        //Assert
        String name = "Vinicius";
        String phone = "936932448";
        String email = "vinicius_franco5@hotmail.com";
        String postalCode = "4200013";
        Contact contact = new Contact(name, email, phone,postalCode);
        //Act
        String result = contact.getName();
        String expectedResult = "Vinicius";
        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void createNewContactGetPhone() {
        //Assert
        String name = "Vinicius";
        String phone = "936932448";
        String email = "vinicius_franco5@hotmail.com";
        String postalCode = "4200013";
        Contact contact = new Contact(name, email, phone,postalCode);
        //Act
        String result = contact.getPhone();
        String expectedResult = "936932448";
        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void createNewContactGetEmail() {
        //Assert
        String name = "Vinicius";
        String phone = "936932448";
        String email = "vinicius_franco5@hotmail.com";
        String postalCode = "4200013";
        Contact contact = new Contact(name, email, phone,postalCode);
        //Act
        String result = contact.getEmail();
        String expectedResult = "vinicius_franco5@hotmail.com";
        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void createNewContactGetPostalCode() {
        //Assert
        String name = "Vinicius";
        String phone = "936932448";
        String email = "vinicius_franco5@hotmail.com";
        String postalCode = "4200013";
        Contact contact = new Contact(name, email, phone,postalCode);
        //Act
        String result = contact.getPostalCode();
        String expectedResult = "4200013";
        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void createNewContactNonValidPhone() {
        //Assert
        String name = "Vinicius";
        String phone = "936932448d";
        String email = "vinicius_franco5@hotmail.com";
        String postalCode = "4200013";
        Contact contact = new Contact(name, email, phone,postalCode);
        //Act
        contact.setPhone(phone);
        String result = contact.getPhone();
        String expectedResult = "* invalid number *";
        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void createNewContactNonValidEmail() {
        //Assert
        String name = "Vinicius";
        String phone = "936932448";
        String email = "vinicius_franco5hotmail.com";
        String postalCode = "4200013";
        Contact contact = new Contact(name, email, phone,postalCode);
        //Act
        contact.setEmail(email);
        String result = contact.getEmail();
        String expectedResult = "* invalid email *";
        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void createNewContactNonValidPostalCode() {
        //Assert
        String name = "Vinicius";
        String phone = "936932448";
        String email = "vinicius_franco5hotmail.com";
        String postalCode = "4200-013";
        Contact contact = new Contact(name, email, phone, postalCode);
        //Act
        contact.setPostalCode(postalCode);
        String result = contact.getPostalCode();
        String expectedResult = "* invalid number *";
        //Assert
        assertEquals(expectedResult, result);
    }
}