
Git Exercise
-------------------------
1)- Create branch master

Create a repository on Bit Bucket and first initial commit (35a997d)
Create a local repository

    % 1 - git init
    % 2 - git clone https://1160165@bitbucket.org/1160165/devops-18-19-atb.git
    
Download the errai-demonstration and move to folder C:\Users\vinic\Documents\Vinicius\Switch\DevOps\Trabalho\devops-18-19-atb\

    % 3 - git add -A
    % 4 - git commit -a -m "add errai demonstration"
    % 5 - git push
    
2)- Create tags in this branch to mark the versions of the application

    % 1 - git add -A
    % 2 - git tag -a v1.2.0 -m "my version 1.2.0"
    % 3 - git push origin v1.2.0
    
3)- Develop new features in branches named after the feature

    % 1 - git branch phone-number
    % 2 - git checkout phone-number
    
Add new Branch to get phone number

    % 3 - git commit -a -m "branch phone number"
    
Add support for a phone field.

    % 4 - git commit -a -m "test"
    
Add unit tests for testing

    % 5 - git commit -a -m "add unit tests for creation of new contacts"
    % 6 - git tag -a v1.3.0 -m "my version 1.3.0"
    
Conclude the feature to add a phone-number on project

    % 7 - git commit -a -m "Fixes#4: add support for a phone field"
    
Checkout to master branch and merge the phone-number branch

    % 8 - git checkout master
    % 9 - git merge phone-number
    % 10 - git push origin v1.3.0

4)- Fix the bug email (create a validation to correct)

    % 1 - git tag -a v1.3.1 -m "my version 1.3.1"
    
Create a new branch fix-invalid-email

    % 2 - git branch fix-invalid-email
    % 3 - git checkout fix-invalid-email
    
Create a method on Class Contact named validateEmail to correct this bug

    % 4 - git commit -a -m "Fixes#5 - create branch for fixing bug to email"
    % 5 - git push origin v1.3.1
    
5)- Test Mercurial Version Control
Before ending the exercise I've tried to implements the Mercurial Version Control and did a commit using the command line:

    % 1 - hg commit -m "teste mercurial version control"
    % 2 - hg push
 
6)- Ending the exercise

    % 1 - git tag -a ca1 -m "git version control exercise"
    % 2 - git commit -a -m "conclude exercise git version control"
    % 3 - git push origin ca1






