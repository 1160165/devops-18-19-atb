
Git Exercise - alternative tecnology
-------------------------

An example implementation of a RESTful Web Service using Spring Boot, Java 8 and Swagger.
This is a Spring Boot project built with Maven 3.3 and Java 8. 

1)- Create branch master

Create a repository on Bit Bucket and first initial commit (35a997d)
Create a local repository

    % 1 - git init
    % 2 - git clone https://1160165@bitbucket.org/1160165/devops-18-19-atb.git
    
Download the restful.phonebook and move to folder C:\Users\vinic\Documents\Vinicius\Switch\DevOps\Trabalho\devops-18-19-atb\ca1

    % 3 - git add -A
    % 4 - git commit -a -m "replace to folder assignment ca1"
    % 5 - git push
    
    
2)- Develop new feature to add a email 

    % 1 - git branch feature-email
    % 2 - git checkout feature-email
  
Add new features to email in Contact Class

    % 3 - git commit -a -m "feature email in Spring Boot framework"
    % 4 - git push origin feature-email

Checkout to master branch and merge the phone-number branch

    % 5 - git checkout master
    % 6 - git merge phone-number
    % 7 - git push origin v1.3.0

What technology setup is required to use the alternative? It requires Maven to build?

Spring Boot framework needs Java 8 and Swagger. Required maven to build.

Does it require a specific IDE?

Don't need for a specific IDE.

What language is used for the client? And for the server?

Java for client and server.




