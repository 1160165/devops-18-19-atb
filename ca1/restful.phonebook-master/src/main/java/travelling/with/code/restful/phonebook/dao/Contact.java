package travelling.with.code.restful.phonebook.dao;

/**
 * A class containing all the useful information of a contact in the phone book.
 * <p/>
 * When a phone book user wants to insert a new contact in the book, he will only be
 * concerned about the information included in this class.
 *
 * @author <a href="mailto:travelling.with.code@gmail.com">Alex</a>
 */
public class Contact {

    private String name;
    private String surname;
    private String phone;
    private String email;

    public Contact(String name, String surname, String phone, String email) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        validateEmail(email);
        this.email = email;
    }

    public Contact(Contact contact) {
        this.name = contact.getName();
        this.surname = contact.getSurname();
        this.phone = contact.getPhone();
        validateEmail(contact.getEmail());
        this.email = contact.getEmail();

    }

    public Contact() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        validateEmail(email);
        return email;
    }

    public void setEmail(String email) {
        validateEmail(email);
        this.email = email;
    }

    public String validateEmail(String email) {
        String EMAIL_REGEX = "^[\\w-\\.+]*[\\w-\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        if (email.matches(EMAIL_REGEX)==true){
            return email;}
        else {
            email = "* invalid email *";

            return email;
        }
    }
}
