Class Assignment 3
===================
Part 1 - Build Tools with Gradle
-------------
## VirtualBox - Install and Experimentation

-------------------

Before doing Class Assignment 3 - Part1, I started by installing VirtualBox on system. Once installed, I also downloaded the ISO file Ubuntu 18.04, and installed it in VirtualBox. After having installed all the necessary tools for this operation, I clone the errai project and I ran some commands:

	 % 1 - ./gradlew provision
	 % 2 - ./gradlew build
     % 3 - ./gradlew startWildfly
	 

After the modifications were completed, opened the link available ''http://192.168.56.100:8080/errai-demonstration-gradle".	 

###CA1

Still in VirtualMachine I made the following command to clone the repositorie and have the informations on virtual machine:

	 git clone https://1160165@bitbucket.org/1160165/devops-18-19-atb.git
	 
Then went to the folder CA1 and then put the following command:

	 % 1 - mvn gwt:run
	 
When placing this command, Virtual Machine asked to install maven, since it had not yet maven installed. I put:

	 % 2 - apt install maven

After installing maven, I made the following command in the CA1 folder:

	 % 3 -./gradlew build
	
The following error appeared	
	
	 Failed to execute goal on project errai-demonstration : Could not resolve dependencies for project org.atb.errai.demos:errai-demonstration:war:1.1.0-SNAPSHOT: Could not find artifact sun.jdk:jconsole:jar:jdk at specified path /usr/lib/jvm/java-8-jdk-amd64/jre/../lib/jconsole.jar

This error happens because Virtual Machine has no graphic interface and can not build with maven. To execute a interface graphic we need to install a complete version of Ubuntu.	
	
##CA2   

In the folder CA2 - part1, and since it is a gradle project I made the following command:
	 
	 % 1 - apt install gradle

After that I went to the build.gradle file to comment on the zip task since I realized that we could not build the gradle with this task. After this modification, I then made the gradle project build with the command:

     % 2 - gradle build

After the build was done, I then proceeded to the execution of the chat, with the following command:

     % 3 - gradle runServer
	 
In IDEA, in the task of 'RunClient' and instead of localhost put 192.168.56.55

     task runClient(type:JavaExec, dependsOn: classes){
     classpath = sourceSets.main.runtimeClasspath

     main = 'basic_demo.ChatClientApp'

     args '192.168.56.55', '59001'
     }
	 
On the command line after these changes I went to the same folder "luis-nogueira-gradle" -> part1 and just wrote the following command:

	 % 4 - gradlew runClient

The chat appeared immediately.

