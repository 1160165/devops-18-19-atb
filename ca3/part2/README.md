Class Assignment 3
===================

Part 3 - Virtualization with Vagrant
-------------

## Vagrant - Introduction to Vagrant

Vagrant is a tool for building and managing virtual machine environments in a single workflow. With an easy-to-use workflow and focus on automation, Vagrant lowers development environment setup time, increases production parity, and makes the "works on my machine" excuse a relic of the past.
Vagrant is designed for everyone as the easiest and fastest way to create a virtualized environment!

### Vagrant - Why ?

Vagrant provides easy to configure, reproducible, and portable work environments built on top of industry-standard technology and controlled by a single consistent workflow to help maximize the productivity and flexibility of you and your team.

To achieve its magic, Vagrant stands on the shoulders of giants. Machines are provisioned on top of VirtualBox, VMware, AWS, or any other provider. Then, industry-standard provisioning tools such as shell scripts, Chef, or Puppet, can automatically install and configure software on the virtual machine.

### Simple experimentation of Vagrant

Before starting Class Assignment 3 - Part2, I started by installing the Vagrant and making the specifications told, by the teacher in the PDF "Lecture 06 -
Introduction to Vagrant "

##Step 1

After installing Vagrant, I went directly to the windows command line and created a folder named "vagrant-project-1" with the following command:

	 % 1 - mkdir vagrant-project-1
	 % 2 - cd vagrant-project-1


After created folder, and so that Vagrant could create the virtual machine, it was necessary to create a VagrantFile with the following command:

	 % 3 - vagrant init envimation/ubuntu-xenial

##Step 2

To add a box to the VagrantFile file, the following command was made:

	 % 1 - vagrant box add envimation/ubuntu-xenial http://www.dei.isep.ipp.pt/~alex/publico/ubuntu-xenial-virtualbox.box

This adds a box with this link to Vagrant. 

Then I started the virtual machine with the following command:

     % 2 - vagrant up

After that, I started the virtual machine session with the following command:
	 
	 % 3 - vagrant ssh

Finally, I turned off my virtual machine with the command:

	 % 4 - vagrant halt

##The goal of Part 2 of this assignment is to use Vagrant to setup a virtual environment to execute the Errai Demonstration Web application

##Step 1

I copied the VagrantFile from the vagrant-multi-demo-master folder to the errai-demonstration-gradle-master folder.
I ran the IDEA as an administrator (so you can make necessary changes) to the errai-demonstration-gradle-master project. 
I ran the following commands from the Read Me file: 

	 % 1 - gradlew clean
	 % 2 - gradlew provision
	 % 3 - gradlew build


With this last command (gradlew build) the build folder was created within the errai-demonstration-gradle-master project. Inside this folder exists a subfolder called 'libs' where the errai-demonstration-gradle-master.war file was created. I changeded the name of file for 'errai-demonstration-gradle.war' because the name had either the same as the one in the vagrantFile file :

	  web.vm.provision "shell", :run => 'always', inline: <<-SHELL
      # We assume that errai-demonstration-gradle.war is located in the host folder that contains the Vagrantfile
      sudo cp /vagrant/errai-demonstration-gradle.war /home/vagrant/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle.war
      # So that the wildfly server always starts
      sudo nohup sh /home/vagrant/wildfly-15.0.1.Final/bin/standalone.sh > /dev/null &
      SHELL


After the .war file was created, I also removed it from the libs folder so that it stays at the same level as the vagrantFile file.


    
##Step 2    

After that, in the file gradle build I put the following plugin;

    id 'io.kristiansen.gradle.PlantUMLPlugin' version '0.0.1'
    
I created the folder 'Assets' and inside I made the creation in plantUml of the expected diagrams.
After creating the folder I put the following commands:

     % 1 - gradlew renderPlantUml
     % 2 - gradlew javadoc
     
I made the task for the copyPlantUml in the gradle build folder as in the previous file.
    
    task copyPlantUml(type: Copy) {
        from 'assets'
        into 'build/docs/javadoc'
        include '**/*.png'}  
        
After that it was just run the following command:
    
    % 3 - gradlew copyPlantUml
    

Then I went to the overview summary file and put the following lines:

    <h2>Class Diagram</h2>
    <img src="ClassDiagram.png"></a><br/>
    
    <h2>Sequence Diagram</h2>
    <img src="SequenceDiagram.png"></a><br/></div>
    </div>

And then went to the javadoc folder to the file called 'index' and see the diagrams you had done online